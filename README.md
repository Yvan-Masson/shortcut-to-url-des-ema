# shortcut-to-URL-DES-EMA

File manager extension that allows creating shortcuts to URLs, so that double
clicking on the resulting shortcut opens the web browser and displays the
webpage.

It can be used with file managers that are compliant with
[DES-EMA specification](https://web.archive.org/web/20180304012237/http://www.nautilus-actions.org/?q=node/377)
(from what I know only PCManFM and PCManFM-QT). Created shortcuts are `.desktop`
files.

It currently provides English and French user interface.


# Installation

- Install following dependencies:
  - zenity
  - Adwaita icon set (this should be theme agnostic but I don't know how to do)

- Copy `shortcut-to-URL-DES-EMA` script to one of your `$PATH` directories and
  make it executable;
- Copy `shortcut-to-url.desktop` to `/usr/local/share/file-manager/actions/` or
  `~/.local/share/file-manager/actions` (you may have to create the subfolders)
- Copy `.mo` translation files your are interested in to
  `/usr/share/locale/*/LC_MESSAGES/` or `/custom-dir/*/LC_MESSAGES`, renaming
  it `shortcut-to-URL-DES-EMA.mo`. If you use a custom directory, adjust
  corresponding variable at the beginning of the script;
- Restart your file manager (you may have to re-open you session).

