# SOME DESCRIPTIVE TITLE.
# This file is put in the public domain.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: yvan@masson-informatique.fr\n"
"POT-Creation-Date: 2019-07-09 15:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: shortcut-to-URL-DES-EMA:28 shortcut-to-url.desktop.template:4
msgid "Create a shortcut to an URL…"
msgstr ""

#: shortcut-to-URL-DES-EMA:33
msgid "Shortcut's name:"
msgstr ""

#: shortcut-to-URL-DES-EMA:36
msgid "Error: a file with this name already exists"
msgstr ""

#: shortcut-to-URL-DES-EMA:40 shortcut-to-URL-DES-EMA:47
msgid "Error: this field can not be empty"
msgstr ""

#: shortcut-to-URL-DES-EMA:44
msgid "URL:"
msgstr ""
